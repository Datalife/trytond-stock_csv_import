======================================
Stock Shipment Out Csv Import Scenario
======================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.stock_csv_import.tests.tools import read_csv_file
    >>> import os
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from decimal import Decimal
    >>> import datetime
    >>> today = datetime.date.today()

Install stock_csv_import::

    >>> config = activate_modules('stock_csv_import')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create Configuration::

    >>> Configuration = Model.get('stock.configuration')
    >>> config = Configuration()
    >>> config.shipment_out_return_csv_headers = "reference,customer.tax_identifier.code,delivery_address.zip,effective_date,warehouse.code,incoming_moves.product.id,incoming_moves.quantity"
    >>> config.save()

Create Customer::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> customer.addresses[0].zip = '86020'
    >>> customer.save()
    >>> identifier = customer.identifiers.new()
    >>> identifier.code = 'IT00972990709'
    >>> identifier.type = 'eu_vat'
    >>> customer.save()

Create Product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product 1'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.purchasable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.save()
    >>> product.template = template
    >>> product.save()

CSV import wizard::

    >>> ShipmentOutReturn = Model.get('stock.shipment.out.return')
    >>> Data = Model.get('ir.model.data')
    >>> data, = Data.find([
    ...     ('module', '=', 'stock_csv_import'),
    ...     ('fs_id', '=', 'wizard_shipment_out_return_csv_import')])
    >>> csv_import = Wizard('stock.shipment.csv_import', action={'id': data.db_id})
    >>> filename = os.path.join(os.path.dirname(__file__), 'out_return.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.form.name = 'CSV Import 1'
    >>> csv_import.execute('import_')
    >>> shipment, = ShipmentOutReturn.find([])
    >>> shipment.effective_date
    datetime.date(2019, 1, 14)
    >>> len(shipment.moves)
    2
    >>> move = shipment.moves[0]
    >>> move.quantity
    15.0
    >>> move.product.name
    'Product 1'
    >>> move = shipment.moves[1]
    >>> move.quantity
    20.0
    >>> move.product.name
    'Product 1'

Check Stock CSV Import::

    >>> IrModel = Model.get('ir.model')
    >>> model, = IrModel.find([('model', '=', 'stock.shipment.out.return')])
    >>> CsvImport = Model.get('stock.csv_import')
    >>> csv_import, = CsvImport.find([])
    >>> csv_import.name
    'CSV Import 1'
    >>> csv_import.model == model
    True
    >>> csv_import.file == read_csv_file(filename)
    True
    >>> csv_import.date == today
    True

CSV import wizard when product does not exist::

    >>> ShipmentOutReturn = Model.get('stock.shipment.out.return')

    >>> csv_import = Wizard('stock.shipment.csv_import', action={'id': data.db_id})
    >>> filename = os.path.join(os.path.dirname(__file__), 'out_return_error.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.execute('import_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot find a Product record with value "5" for CSV file line number "None". - 