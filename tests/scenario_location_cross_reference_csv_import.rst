===========================================
Location Cross Reference CSV Import Scenario
===========================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.stock_csv_import.tests.tools import read_csv_file
    >>> import os
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from decimal import Decimal
    >>> import datetime
    >>> today = datetime.date.today()

Install stock_csv_import::

    >>> config = activate_modules(['stock_csv_import',
    ...     'stock_location_cross_reference'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create Configuration::

    >>> Configuration = Model.get('stock.configuration')
    >>> config = Configuration()
    >>> config.shipment_in_csv_headers = "reference,supplier.name,contact_address,warehouse.name,effective_date,incoming_moves.product.name,incoming_moves.quantity"
    >>> config.save()

Create Party::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> address, = supplier.addresses
    >>> address.street = 'Street 1'
    >>> supplier.save()

Create Product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product1 = Product()
    >>> product2 = Product()
    >>> template1 = ProductTemplate()
    >>> template1.name = 'Product 1'
    >>> template1.default_uom = unit
    >>> template1.type = 'goods'
    >>> template1.purchasable = True
    >>> template1.list_price = Decimal('10')
    >>> template1.cost_price = Decimal('5')
    >>> template1.cost_price_method = 'fixed'
    >>> template1.save()
    >>> product1.template = template1
    >>> product1.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> lost_found_loc, = Location.find([('type', '=', 'lost_found')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> internal_loc = Location(name='Internal', type='storage')
    >>> internal_loc.save()
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> warehouse_loc.name = 'Warehouse 1'
    >>> warehouse_loc.save()

Create other warehouse::

    >>> in_loc2 = Location(name='IN Zone 2')
    >>> in_loc2.type = 'storage'
    >>> in_loc2.save()
    >>> out_loc2 = Location(name='OUT Zone 2')
    >>> out_loc2.type = 'storage'
    >>> out_loc2.save()
    >>> sto_loc2 = Location(name='STO Zone 2')
    >>> sto_loc2.type = 'storage'
    >>> sto_loc2.save()
    >>> wh2 = Location(name='Warehouse 2')
    >>> wh2.code = 'WH2'
    >>> wh2.type = 'warehouse'
    >>> wh2.input_location = in_loc2
    >>> wh2.output_location = out_loc2
    >>> wh2.storage_location = sto_loc2
    >>> wh2.save()

Create Location Cross Reference::

    >>> CrossReference = Model.get('stock.location.cross_reference')
    >>> cross_reference = CrossReference()
    >>> cross_reference.party = supplier
    >>> cross_reference.location = wh2
    >>> cross_reference.name = 'WH 2'
    >>> cross_reference.save()

Create Move::

    >>> Move = Model.get('stock.move')
    >>> move = Move()
    >>> move.product = product1
    >>> move.quantity = 1
    >>> move.from_location = internal_loc
    >>> move.to_location = storage_loc
    >>> move.currency = company.currency
    >>> move.save()

Create Shipment In::

    >>> ShipmentIn = Model.get('stock.shipment.in')
    >>> shipment_in = ShipmentIn()
    >>> shipment_in.effective_date = today
    >>> shipment_in.supplier = supplier
    >>> shipment_in.warehouse = warehouse_loc
    >>> shipment_in.reference = 'Reference 1'
    >>> shipment_in.company = company
    >>> shipment_in.moves.append(move)
    >>> shipment_in.save()
    >>> len(shipment_in.moves)
    1

CSV import shipment in wizard::

    >>> Data = Model.get('ir.model.data')
    >>> data, = Data.find([
    ...     ('module', '=', 'stock_csv_import'),
    ...     ('fs_id', '=', 'wizard_shipment_in_csv_import')])
    >>> csv_import = Wizard('stock.shipment.csv_import', action={'id': data.db_id})
    >>> filename = os.path.join(os.path.dirname(__file__),
    ...     'location_cross_reference_in.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.form.store_file = False
    >>> csv_import.execute('import_')
    >>> shipment_in.reload()
    >>> len(shipment_in.moves)
    1
    >>> shipments = ShipmentIn.find([])
    >>> len(shipments)
    2
    >>> new_shipment = sorted(shipments, key=lambda s: s.id)[-1]
    >>> new_shipment.moves[0].to_location == wh2.input_location
    True

When location cross reference does not exist::

    >>> csv_import = Wizard('stock.shipment.csv_import', action={'id': data.db_id})
    >>> filename = os.path.join(os.path.dirname(__file__),
    ...     'location_cross_reference_error.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.form.store_file = False
    >>> csv_import.execute('import_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot find a Warehouse record with value "Warehouse Error" for CSV file line number "1". - 