=====================================
Stock Shipment In Csv Import Scenario
=====================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from decimal import Decimal
    >>> import datetime
    >>> import os
    >>> today = datetime.date.today()
    >>> from trytond.modules.stock_csv_import.tests.tools import read_csv_file
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.tests.tools import activate_modules


Install stock_shipment_in_csv_import::

    >>> config = activate_modules('stock_csv_import')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create Supplier::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier 1')
    >>> address, = supplier.addresses
    >>> address.street = 'Street 1'
    >>> supplier.save()

Create Warehouse::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> warehouse_loc.name = 'Warehouse 1'
    >>> warehouse_loc.save()

Create Product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product 1'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.purchasable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> lost_found_loc, = Location.find([('type', '=', 'lost_found')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> internal_loc = Location(name='Internal', type='storage')
    >>> internal_loc.save()

Create Move::

    >>> Move = Model.get('stock.move')
    >>> move = Move()
    >>> move.product = product
    >>> move.quantity = 1
    >>> move.from_location = internal_loc
    >>> move.to_location = storage_loc
    >>> move.currency = company.currency
    >>> move.save()

Create Shipment In::

    >>> ShipmentIn = Model.get('stock.shipment.in')
    >>> shipment_in = ShipmentIn()
    >>> shipment_in.effective_date = today
    >>> shipment_in.supplier = supplier
    >>> shipment_in.warehouse = warehouse_loc
    >>> shipment_in.reference = 'Reference 1'
    >>> shipment_in.company = company
    >>> shipment_in.moves.append(move)
    >>> shipment_in.save()
    >>> shipment_in_waiting, = ShipmentIn.duplicate([shipment_in])
    >>> shipment_in_waiting.reference = 'Reference 2'
    >>> shipment_in_waiting.save()
    >>> shipment_in_waiting.click('receive')

Create Configuration::

    >>> Configuration = Model.get('stock.configuration')
    >>> config = Configuration()
    >>> config.shipment_in_csv_headers = "reference,supplier.name,contact_address,warehouse.name,effective_date,incoming_moves.product.name,incoming_moves.quantity"
    >>> config.save()

Execute Wizard::
    >>> Data = Model.get('ir.model.data')
    >>> data, = Data.find([
    ...     ('module', '=', 'stock_csv_import'),
    ...     ('fs_id', '=', 'wizard_shipment_in_csv_import')])
    >>> csv_import = Wizard('stock.shipment.csv_import', action={'id': data.db_id})
    >>> filename = os.path.join(os.path.dirname(__file__), 'in.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.form.name = 'CSV Import 1'
    >>> csv_import.execute('import_')
    >>> shipment_in.reload()
    >>> shipment_in.effective_date
    datetime.date(2016, 9, 21)
    >>> len(Move.find())
    5
    >>> csv_import = Wizard('stock.shipment.csv_import', action={'id': data.db_id})
    >>> filename = os.path.join(os.path.dirname(__file__), 'in.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.form.store_file = False
    >>> csv_import.execute('import_')
    >>> len(Move.find())
    5

Check Stock CSV Import::

    >>> IrModel = Model.get('ir.model')
    >>> model, = IrModel.find([('model', '=', 'stock.shipment.in')])
    >>> CsvImport = Model.get('stock.csv_import')
    >>> csv_import, = CsvImport.find([])
    >>> csv_import.name
    'CSV Import 1'
    >>> csv_import.model == model
    True
    >>> csv_import.file == read_csv_file(filename)
    True
    >>> csv_import.date == today
    True

When product does not exist::

    >>> csv_import = Wizard('stock.shipment.csv_import', action={'id': data.db_id})
    >>> filename = os.path.join(os.path.dirname(__file__), 'in_error.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.execute('import_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot find a Product record with value "Product 2" for CSV file line number "None". - 