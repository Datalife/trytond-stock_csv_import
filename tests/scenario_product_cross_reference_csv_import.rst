===========================================
Product Cross Reference CSV Import Scenario
===========================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.stock_csv_import.tests.tools import read_csv_file
    >>> import os
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from decimal import Decimal
    >>> import datetime
    >>> today = datetime.date.today()

Install stock_csv_import::

    >>> config = activate_modules(['stock_csv_import', 'product_cross_reference', 'stock_location_cross_reference'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create Configuration::

    >>> Configuration = Model.get('stock.configuration')
    >>> config = Configuration()
    >>> config.shipment_out_return_csv_headers = "reference,customer.tax_identifier.code,delivery_address.zip,effective_date,warehouse.code,incoming_moves.product.name,incoming_moves.quantity"
    >>> config.shipment_in_csv_headers = "reference,supplier.name,contact_address,warehouse.name,effective_date,incoming_moves.product.name,incoming_moves.quantity"
    >>> config.save()

Create Parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> customer.addresses[0].zip = '86020'
    >>> customer.save()
    >>> identifier = customer.identifiers.new()
    >>> identifier.code = 'IT00972990709'
    >>> identifier.type = 'eu_vat'
    >>> customer.save()
    >>> supplier = Party(name='Supplier')
    >>> address, = supplier.addresses
    >>> address.street = 'Street 1'
    >>> supplier.save()

Create Products::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product1 = Product()
    >>> product2 = Product()
    >>> template1 = ProductTemplate()
    >>> template1.name = 'Product 1'
    >>> template1.default_uom = unit
    >>> template1.type = 'goods'
    >>> template1.purchasable = True
    >>> template1.list_price = Decimal('10')
    >>> template1.cost_price = Decimal('5')
    >>> template1.cost_price_method = 'fixed'
    >>> template1.save()
    >>> product1.template = template1
    >>> product1.save()
    >>> template2 = ProductTemplate()
    >>> template2.name = 'Product 2'
    >>> template2.default_uom = unit
    >>> template2.type = 'goods'
    >>> template2.purchasable = True
    >>> template2.list_price = Decimal('10')
    >>> template2.cost_price = Decimal('5')
    >>> template2.cost_price_method = 'fixed'
    >>> template2.save()
    >>> product2.template = template2
    >>> product2.save()

Create Product Cross Reference::

    >>> CrossReference = Model.get('product.cross_reference')
    >>> cross_reference = CrossReference()
    >>> cross_reference.party = customer
    >>> cross_reference.product = product1
    >>> cross_reference.name = 'Product Ref 1'
    >>> cross_reference.save()
    >>> cross_reference2 = CrossReference()
    >>> cross_reference2.party = supplier
    >>> cross_reference2.product = product2
    >>> cross_reference2.name = 'Product Ref 2'
    >>> cross_reference2.save()


CSV import shipment out return wizard::

    >>> ShipmentOutReturn = Model.get('stock.shipment.out.return')
    >>> Data = Model.get('ir.model.data')
    >>> data, = Data.find([
    ...     ('module', '=', 'stock_csv_import'),
    ...     ('fs_id', '=', 'wizard_shipment_out_return_csv_import')])
    >>> csv_import = Wizard('stock.shipment.csv_import', action={'id': data.db_id})
    >>> filename = os.path.join(os.path.dirname(__file__),
    ...     'product_cross_reference_out_return.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.form.store_file = False
    >>> csv_import.execute('import_')
    >>> shipment, = ShipmentOutReturn.find([])
    >>> move, = shipment.moves
    >>> move.product == product1
    True

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> lost_found_loc, = Location.find([('type', '=', 'lost_found')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> internal_loc = Location(name='Internal', type='storage')
    >>> internal_loc.save()
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> warehouse_loc.name = 'Warehouse 1'
    >>> warehouse_loc.save()

Create Move::

    >>> Move = Model.get('stock.move')
    >>> move = Move()
    >>> move.product = product1
    >>> move.quantity = 1
    >>> move.from_location = internal_loc
    >>> move.to_location = storage_loc
    >>> move.currency = company.currency
    >>> move.save()

Create Shipment In::

    >>> ShipmentIn = Model.get('stock.shipment.in')
    >>> shipment_in = ShipmentIn()
    >>> shipment_in.effective_date = today
    >>> shipment_in.supplier = supplier
    >>> shipment_in.warehouse = warehouse_loc
    >>> shipment_in.reference = 'Reference 1'
    >>> shipment_in.company = company
    >>> shipment_in.moves.append(move)
    >>> shipment_in.save()
    >>> len(shipment_in.moves)
    1

CSV import shipment in wizard::

    >>> data, = Data.find([
    ...     ('module', '=', 'stock_csv_import'),
    ...     ('fs_id', '=', 'wizard_shipment_in_csv_import')])
    >>> csv_import = Wizard('stock.shipment.csv_import', action={'id': data.db_id})
    >>> filename = os.path.join(os.path.dirname(__file__),
    ...     'product_cross_reference_in.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.form.store_file = False
    >>> csv_import.execute('import_')
    >>> shipment_in.reload()
    >>> len(shipment_in.moves)
    2
    >>> shipment_in.moves[0].product == product2
    True

When product cross reference does not exist::

    >>> csv_import = Wizard('stock.shipment.csv_import', action={'id': data.db_id})
    >>> filename = os.path.join(os.path.dirname(__file__),
    ...     'product_cross_reference_error.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.execute('import_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot find a Product record with value "Product Error" for CSV file line number "None". - 