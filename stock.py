# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import os
import csv
import tempfile
from datetime import datetime
from collections import OrderedDict
from trytond.pool import Pool
from trytond.model import fields, ModelView, ModelSQL
from trytond.pyson import Eval, Not, Bool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateAction, Button
from io import StringIO
import ssl
from ftplib import FTP, FTP_TLS
from trytond.config import config
from os import path
from urllib.parse import urlparse

__all__ = ['ShipmentCSVMixin', 'CsvImport', 'CsvImportStart', 'StockCsvImport',
    'CrossReferenceMixin', 'ShipmentCSVProductMixin',
    'ShipmentCSVLocationMixin']


if config.getboolean('stock_csv_import', 'filestore', default=True):
    file_id = 'file_id'
    store_prefix = config.get('stock_csv_import', 'store_prefix',
        default=None)
else:
    file_id = None
    store_prefix = None


class ShipmentCSVMixin(object):

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._error_messages.update({
            'csv_date_format_error': 'The date format must be "YYYY-MM-DD".',
            'csv_relation_not_found':
                'Cannot find a %s record with value "%s" for '
                'CSV file line number "%s".',
            'csv_import_error': 'Error while importing file "%s": "%s"',
        })

    _csv_move_field = ''

    @classmethod
    def _get_csv_field_extra_domain(cls, field_name, data, row=[]):
        return []

    @classmethod
    def _get_csv_field_value(cls, Model, field_name, field_value,
            data=None, row=[], row_number=None, user_error=True):
        pool = Pool()
        splitted_field = []
        if '.' in field_name:
            splitted_field = field_name.split('.')
            fieldname = splitted_field[0]
        else:
            fieldname = field_name
        if getattr(Model, fieldname, False):
            if isinstance(Model._fields[fieldname], fields.Many2One):
                field = Model._fields[fieldname]
                relation = field.get_target().__name__
                RelModel = pool.get(relation)
                domain = cls._get_csv_field_extra_domain(field.name, data,
                    row=row)
                if splitted_field:
                    domain.append((".".join(splitted_field[1:]), '=',
                        field_value))
                else:
                    domain.append(('rec_name', '=', field_value))
                value = RelModel.search(domain)
                if user_error and field_value and not value:
                    cls.raise_user_error('csv_relation_not_found', (
                        Model.fields_get(fields_names=[fieldname]
                            )[fieldname]['string'],
                        field_value, row_number))
                return value[0] if value else None
            elif isinstance(Model._fields[fieldname], fields.Date):
                try:
                    return datetime.strptime(field_value, "%Y-%m-%d").date()
                except ValueError:
                    cls.raise_user_error('csv_date_format_error')
        return field_value

    @classmethod
    def _get_csv_key(cls):
        return ('effective_date', 'reference', )

    @classmethod
    def _get_csv_headers(cls):
        Config = Pool().get('stock.configuration')
        config = Config(1)
        field_name = cls.__name__[6:].replace('.', '_')
        headers = getattr(config, field_name + '_csv_headers', [])
        if headers:
            headers = headers.strip(" ").split(",")
        return headers or []

    @classmethod
    def import_csv(cls, csv_file, csv_delimiter=',', name=None,
            store_file=False):
        pool = Pool()
        Company = pool.get('company.company')
        Move = pool.get('stock.move')
        CsvImport = pool.get('stock.csv_import')
        Model = pool.get('ir.model')
        old_shipment = None
        to_del = []
        shipments = []
        file = csv_file

        config_header = cls._get_csv_headers()

        # On python3 we must convert the binary file to string
        if hasattr(csv_file, 'decode'):
            csv_file = csv_file.decode(errors='ignore')
        csv_file = StringIO(csv_file)
        csv_file = csv.reader(csv_file, delimiter=csv_delimiter)
        headers = next(csv_file)
        unique_key = dict([
            (d.split('.')[0], i) for i, d in enumerate(config_header)
            if d.split('.')[0] in cls._get_csv_key()])

        data = OrderedDict()
        assert len(headers) == len(config_header)
        row_number = 0
        for row in csv_file:
            row_number += 1
            _key = tuple(row[value] for value in sorted(
                unique_key.values()))
            if not data.get(_key):
                data.setdefault(_key, {cls._csv_move_field: []})
            data[_key][cls._csv_move_field].append({})
            for i, column in enumerate(config_header):
                if column.startswith(cls._csv_move_field):
                    _, column = column.split('.', 1)
                    data[_key][cls._csv_move_field][-1][column] = row[i]
                else:
                    if not row[i]:
                        continue
                    column_no_dot = column.split(
                        '.')[0] if '.' in column else column
                    row[i] = cls._get_csv_field_value(cls, column, row[i],
                        data=data[_key], row=row, row_number=row_number)
                    data[_key][column_no_dot] = row[i]

        company = Company(Transaction().context['company'])
        for k, v in data.items():
            shipment = None
            if any(item for item in k):
                old_shipment = None
                domain = [
                    ('company', '=', company.id), ] + [
                    (k, '=', v.get(k)) for k in cls._get_csv_key()]
                shipment = cls.search(domain, limit=1)
                if shipment:
                    shipment = cls._check_csv_shipment(shipment)
                    if not shipment:
                        continue
                    if v[cls._csv_move_field][0]:
                        to_del.extend(list(getattr(shipment,
                            cls._csv_move_field, [])))
            else:
                # key values are not given so move is from previous record
                shipment = old_shipment
            if not shipment:
                shipment = cls()
            moves = []
            _move_data = v.pop(cls._csv_move_field)
            if not old_shipment:
                for k2, v2 in v.items():
                    setattr(shipment, k2, v2)

            for move_values in _move_data:
                if not move_values:
                    continue

                move = Move(
                    company=company,
                    currency=company.currency,
                    effective_date=shipment.effective_date)
                for move_key, move_value in move_values.items():
                    _value = cls._get_csv_field_value(
                        Move, move_key, move_value, data=v)
                    move_field = move_key.split('.')[0]
                    setattr(move, move_field, _value)
                shipment._set_csv_move_locations(move)
                move.quantity = float(move.quantity)
                move.on_change_product()
                moves.append(move)

            if not old_shipment:
                setattr(shipment, cls._csv_move_field, moves)
                shipments.append(shipment)
                old_shipment = shipment
            else:
                setattr(shipment, cls._csv_move_field, list(
                    shipment.incoming_moves) + moves)

        if store_file:
            model, = Model.search([('model', '=', cls.__name__)])
            csv_import = CsvImport(
                name=name,
                model=model,
                file=file)
            csv_import.save()
        if shipments:
            cls.save(shipments)
        if to_del:
            Move.delete(to_del)

        return shipments

    def _set_csv_move_locations(self, move):
        """Set locations to move"""
        pass

    @classmethod
    def _check_csv_shipment(cls, shipments):
        value = shipments[0]
        if value.state == 'draft':
            return value

    @classmethod
    def cron_import_csv_from_ftp(cls, folder_path='', csv_delimiter=',',
            store_file=False):
        url = urlparse(config.get('ftp_shipment', 'url'))
        backup_folder = config.get('ftp_shipment', 'backup')
        tls = config.getboolean('ftp_shipment', 'tls', default=False)
        tls_secure = config.getboolean('ftp_shipment', 'tls_secure',
            default=True)

        if tls:
            ftp = FTP_TLS()
            ftp.ssl_version = ssl.PROTOCOL_SSLv23
            ftp.connect(url.hostname)
            ftp.login(url.username, url.password, secure=tls_secure)
        else:
            ftp = FTP(url.hostname)
            ftp.login(url.username, url.password)
        ftp.cwd(folder_path)
        imported_files = []
        try:
            filename = ''
            for filename in ftp.nlst('*.csv'):
                fileno, fname = tempfile.mkstemp('.csv', 'tryton_')
                handle = open(fname, 'wb')
                res = ftp.retrbinary('RETR %s' % filename,
                    callback=handle.write)
                handle.close()
                cls.import_csv(open(fname, 'rb').read(),
                    csv_delimiter=csv_delimiter, name=filename,
                    store_file=store_file)
                imported_files.append(filename)
        except Exception as e:
            ftp.quit()
            with Transaction().new_transaction():
                cls.raise_user_error('csv_import_error', (filename,
                    getattr(e, 'message', e)))

        if backup_folder not in ftp.nlst():
            ftp.mkd(backup_folder)
        for filename in imported_files:
            ftp.rename(filename, path.join(backup_folder, filename))

        ftp.quit()

    @classmethod
    def cron_import_csv_from_folder(cls, folder_path, csv_delimiter=',',
            store_file=False):
        imported_files = []
        try:
            for filename in os.listdir(folder_path):
                if not filename.endswith('.csv'):
                    continue
                fname = os.path.join(folder_path, filename)
                cls.import_csv(open(fname, 'rb').read(),
                    csv_delimiter=csv_delimiter, name=filename,
                    store_file=store_file)
                imported_files.append(filename)
        except Exception as e:
            with Transaction().new_transaction():
                cls.raise_user_error('csv_import_error', (filename, e.message))

        if not imported_files:
            return

        if not os.path.isdir(os.path.join(folder_path, 'backup')):
            os.mkdir(path)
        for filename in imported_files:
            os.rename(os.path.join(folder_path, filename),
                os.path.join(folder_path, 'backup', filename))


class CrossReferenceMixin(object):

    @classmethod
    def _get_cross_reference_model(cls):
        return None

    @classmethod
    def _get_models(cls):
        return {}

    @classmethod
    def _get_cross_reference_pattern(cls, data):
        pass

    @classmethod
    def _get_csv_field_value(cls, Model, field_name, field_value,
            data=None, row=[], row_number=None, user_error=True):
        models = cls._get_models()
        if '.' in field_name:
            splitted_field = field_name.split('.')
            fieldname = splitted_field[0]
        else:
            fieldname = field_name
        if fieldname in models.keys():
            user_error = False

        value = super()._get_csv_field_value(Model, field_name, field_value,
            data=data, row=row, row_number=row_number, user_error=user_error)

        if fieldname in models.keys() and not value:
            if not splitted_field or (splitted_field and
                    splitted_field[1] in ('name', 'code')):
                CrossReference, dest_field = models[fieldname]
                pattern = cls._get_cross_reference_pattern(data)
                if splitted_field:
                    pattern[splitted_field[1]] = field_value
                else:
                    pattern['rec_name'] = field_value
                cross_reference = CrossReference._get_reference(pattern)
                if not cross_reference:
                    cls.raise_user_error('csv_relation_not_found', (
                        Model.fields_get(fields_names=[fieldname]
                            )[fieldname]['string'],
                        field_value, row_number))
                return getattr(cross_reference, dest_field)

        return value


class ShipmentCSVProductMixin(CrossReferenceMixin):

    @classmethod
    def _get_models(cls):
        models = super()._get_models()
        models['product'] = (Pool().get('product.cross_reference'), 'product')
        return models


class ShipmentCSVLocationMixin(CrossReferenceMixin):

    @classmethod
    def _get_models(cls):
        models = super()._get_models()
        models['warehouse'] = (Pool().get('stock.location.cross_reference'),
            'location')
        return models


class CsvImport(Wizard):
    """Shipment CSV Import"""
    __name__ = 'stock.shipment.csv_import'

    start = StateView(
        'stock.shipment.csv_import.start',
        'stock_csv_import.csv_import_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Import', 'import_', 'tryton-ok', default=True),
        ])
    # define a default action, but it will be changed on execution time
    import_ = StateAction('stock.act_shipment_out_form')

    def default_start(self, fields):
        Modeldata = Pool().get('ir.model.data')
        actions = {
            Modeldata.get_id('stock_csv_import',
                'wizard_shipment_out_return_csv_import'):
                    'stock.shipment.out.return',
            Modeldata.get_id('stock_csv_import',
                'wizard_shipment_in_csv_import'): 'stock.shipment.in',
            Modeldata.get_id('stock_csv_import',
                'wizard_shipment_internal_csv_import'):
                    'stock.shipment.internal'
        }
        _type = actions.get(Transaction().context['action_id'], None)
        return {
            'shipment_type': _type,
            'show_type': not _type
        }

    @classmethod
    def get_shipment_model(cls, Modeldata):
        return {}

    def _do_import(self):
        pool = Pool()
        Shipment = pool.get(self.start.shipment_type)
        csv_file = self.start.csv_file
        return Shipment.import_csv(csv_file,
            csv_delimiter=str(self.start.delimiter),
            name=self.start.name, store_file=self.start.store_file)

    def do_import_(self, action):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        Action = pool.get('ir.action')

        shipments = self._do_import()

        action_id = Action.get_action_id(ModelData.get_id('stock',
            'act_%s_form' % self.start.shipment_type[6:].replace('.', '_')))
        action = Action(action_id)
        data = {'res_id': [i.id for i in shipments] or []}
        return Action.get_action_values(action.type, [action.id])[0], data

    def _get_model(self):
        pass


class CsvImportStart(ModelView):
    """Import CSV View"""
    __name__ = 'stock.shipment.csv_import.start'

    csv_file = fields.Binary('File', required=True)
    show_type = fields.Boolean('Show type')
    shipment_type = fields.Selection([
            ('stock.shipment.out.return', 'Out return'),
            ('stock.shipment.in', 'In'),
            ('stock.shipment.internal', 'Internal')
        ], 'Shipment type', required=True,
        states={
            'invisible': Not(Bool(Eval('show_type')))
        }, depends=['show_type'])
    delimiter = fields.Selection([
        (',', ','),
        (';', ';')], 'Delimiter', required=True)
    store_file = fields.Boolean('Store File')
    name = fields.Char('Name',
        states={
            'required': Bool(Eval('store_file'))},
        depends=['store_file'])

    @staticmethod
    def default_delimiter():
        return ','

    @staticmethod
    def default_store_file():
        return True


class StockCsvImport(ModelSQL, ModelView):
    """Import CSV"""
    __name__ = 'stock.csv_import'

    name = fields.Char('Name', required=True)
    model = fields.Many2One('ir.model', 'Model', required=True, readonly=True,
        domain=[
            ('model', 'in', [
                'stock.shipment.in',
                'stock.shipment.out.return',
                'stock.shipment.internal'
            ])
        ])
    date = fields.Function(fields.Date('Date'), 'get_date')
    file = fields.Binary('File', required=True, readonly=True,
        file_id=file_id, store_prefix=store_prefix)
    file_id = fields.Char('File ID', readonly=True)

    def get_date(self, name):
        return self.create_date.date()
