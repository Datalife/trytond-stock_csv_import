# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import configuration
from . import stock
from . import shipment


def register():
    Pool.register(
        configuration.Configuration,
        stock.CsvImportStart,
        stock.StockCsvImport,
        shipment.ShipmentOutReturn,
        shipment.ShipmentIn,
        shipment.ShipmentInternal,
        module='stock_csv_import', type_='model')
    Pool.register(
        stock.CsvImport,
        module='stock_csv_import', type_='wizard')
    Pool.register(
        shipment.ShipmentOutReturnProduct,
        shipment.ShipmentInProduct,
        module='stock_csv_import', type_='model',
        depends=['product_cross_reference'])
    Pool.register(
        shipment.ShipmentInLocation,
        module='stock_csv_import', type_='model',
        depends=['stock_location_cross_reference'])
